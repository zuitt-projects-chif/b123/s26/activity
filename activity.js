const http = require("http");

http
  .createServer((req, res) => {
    console.log(
      `${new Date().toLocaleTimeString()} [${req.method}] - ${req.url}`
    );
    switch (req.url) {
      case "/":
        res.writeHead(200, { "Content-Type": "text/plain" });
        return res.end("Welcome to our Page");
      case "/login":
        res.writeHead(200, { "Content-Type": "text/plain" });
        return res.end(
          "Welcome to the Login Page. Please log in your credentials"
        );
      case "/register":
        res.writeHead(200, { "Content-Type": "text/plain" });
        return res.end(
          "Welcome to the Register Page. Please register your details"
        );
      default:
        res.writeHead(404, { "Content-Type": "text/plain" });
        return res.end("Resource not found");
    }
  })
  .listen(8000);

console.log("Server running at localhost:8000");
